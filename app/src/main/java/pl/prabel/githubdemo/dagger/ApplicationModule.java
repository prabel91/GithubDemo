package pl.prabel.githubdemo.dagger;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import pl.prabel.githubdemo.MainApplication;

@Module(
        injects = MainApplication.class,
        includes = {
                BaseModule.class,
                DaoModule.class
        },
        library = true
)
public class ApplicationModule {

    private final MainApplication mApplication;

    public ApplicationModule(MainApplication application) {
        mApplication = application;
    }

    @Provides
    @Singleton
    @ForApplication
    Context provideApplicationContext() {
        return mApplication;
    }
}
