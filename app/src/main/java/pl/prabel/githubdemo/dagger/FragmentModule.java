package pl.prabel.githubdemo.dagger;


import dagger.Module;

@Module(
        injects = {},
        addsTo = ApplicationModule.class,
        library = true,
        complete = false
)
public class FragmentModule {

    public FragmentModule() {}

}
