package pl.prabel.githubdemo.dagger;

import dagger.ObjectGraph;

public interface GraphProvider {
    public ObjectGraph getObjectGraph();
}
