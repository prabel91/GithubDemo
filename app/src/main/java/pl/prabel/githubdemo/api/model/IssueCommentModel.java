package pl.prabel.githubdemo.api.model;

import com.google.common.base.Objects;

import java.util.Date;

import javax.annotation.Nonnull;

import pl.prabel.githubdemo.helper.FormatHelper;
import pl.prabel.githubdemo.presenter.issue.BaseAdapterItem;

public class IssueCommentModel extends BaseAdapterItem {
    private final User user;
    private final String body;
    private final Date createdAt;

    public IssueCommentModel(User user, String body, Date createdAt) {
        this.user = user;
        this.body = body;
        this.createdAt = createdAt;
    }

    public User getUser() {
        return user;
    }

    public String getBody() {
        return body;
    }

    public String getCreatedAt() {
        try {
            return FormatHelper.CORRECT_FORMAT.format(createdAt);
        } catch (Exception e) {
            return createdAt.toString();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof IssueCommentModel)) return false;
        IssueCommentModel that = (IssueCommentModel) o;
        return Objects.equal(user, that.user) &&
                Objects.equal(body, that.body) &&
                Objects.equal(createdAt, that.createdAt);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(user, body, createdAt);
    }

    @Override
    public int getItemViewType() {
        return BaseAdapterItem.COMMENT_ITEM_TYPE;
    }

    @Override
    public boolean matches(@Nonnull BaseAdapterItem item) {
        return equals(item);
    }

    @Override
    public boolean same(@Nonnull BaseAdapterItem item) {
        return equals(item);
    }
}
