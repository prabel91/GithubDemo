package pl.prabel.githubdemo.api.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.appunite.detector.SimpleDetector;
import com.google.common.base.Objects;

import java.util.Date;

import javax.annotation.Nonnull;

import pl.prabel.githubdemo.helper.FormatHelper;

public class IssueModel implements SimpleDetector.Detectable<IssueModel>, Parcelable {


    private final String id;
    private final String title;
    private final String body;
    private final Date createdAt;
    private final User user;
    private final int number;

    public IssueModel(String id, String title, String body, Date createdAt, User user, int number) {
        this.id = id;
        this.title = title;
        this.body = body;
        this.createdAt = createdAt;
        this.user = user;
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    public Date getCreateAtDate() {
        return createdAt;
    }

    public String getCreatedAt() {
        try {
            return FormatHelper.CORRECT_FORMAT.format(createdAt);
        } catch (Exception e) {
//            return createdAt;
            return createdAt.toString();
        }
    }

    public User getUser() {
        return user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof IssueModel)) return false;
        IssueModel that = (IssueModel) o;
        return Objects.equal(id, that.id) &&
                Objects.equal(title, that.title) &&
                Objects.equal(body, that.body) &&
                Objects.equal(createdAt, that.createdAt) &&
                Objects.equal(user, that.user);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, title, body, createdAt, user);
    }

    @Override
    public boolean matches(@Nonnull IssueModel item) {
        return equals(item);
    }

    @Override
    public boolean same(@Nonnull IssueModel item) {
        return equals(item);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.title);
        dest.writeString(this.body);
        dest.writeLong(createdAt != null ? createdAt.getTime() : -1);
        dest.writeParcelable(this.user, 0);
        dest.writeInt(this.number);
    }

    protected IssueModel(Parcel in) {
        this.id = in.readString();
        this.title = in.readString();
        this.body = in.readString();
        long tmpCreatedAt = in.readLong();
        this.createdAt = tmpCreatedAt == -1 ? null : new Date(tmpCreatedAt);
        this.user = in.readParcelable(User.class.getClassLoader());
        this.number = in.readInt();
    }

    public static final Parcelable.Creator<IssueModel> CREATOR = new Parcelable.Creator<IssueModel>() {
        public IssueModel createFromParcel(Parcel source) {
            return new IssueModel(source);
        }

        public IssueModel[] newArray(int size) {
            return new IssueModel[size];
        }
    };
}
