package pl.prabel.githubdemo.api;

import com.google.common.collect.ImmutableList;

import pl.prabel.githubdemo.api.model.IssueCommentModel;
import pl.prabel.githubdemo.api.model.IssueModel;
import pl.prabel.githubdemo.api.model.RepoModel;
import retrofit.client.Response;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Path;
import rx.Observable;

public interface ApiService {

    @GET("/authorizations")
    Observable<Response> authorizations();

    @GET("/user/repos")
    Observable<ImmutableList<RepoModel>> getRepositories();

    @GET("/repos/{user}/{repo}/issues")
    Observable<ImmutableList<IssueModel>> getIssuesForRepo(@Path("user") String user, @Path("repo") String repoName);

    @GET("/repos/{user}/{repo}/issues/{issue}/comments")
    Observable<ImmutableList<IssueCommentModel>> getCommentForIssue(@Path("user") String user,
                                                                    @Path("repo") String repoName,
                                                                    @Path("issue") String issueNumber);
}