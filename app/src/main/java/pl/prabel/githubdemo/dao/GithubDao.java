package pl.prabel.githubdemo.dao;

import android.support.annotation.NonNull;

import com.appunite.rx.ObservableExtensions;
import com.appunite.rx.ResponseOrError;
import com.appunite.rx.functions.BothParams;
import com.appunite.rx.functions.ThreeParams;
import com.appunite.rx.operators.MoreOperators;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableList;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.inject.Singleton;

import pl.prabel.githubdemo.api.ApiService;
import pl.prabel.githubdemo.api.model.IssueCommentModel;
import pl.prabel.githubdemo.api.model.IssueModel;
import pl.prabel.githubdemo.api.model.RepoModel;
import rx.Observable;
import rx.Scheduler;
import rx.subjects.PublishSubject;

@Singleton
public class GithubDao {

    @NonNull
    private final LoadingCache<BothParams<String, String>, RepoDao> cache;
    @NonNull
    private final LoadingCache<ThreeParams<String, String, String>, IsuueDao> issueCache;
    @NonNull
    private final ApiService apiService;
    @NonNull
    private final Scheduler networkScheduler;
    @NonNull
    private final Scheduler uiScheduler;

    @NonNull
    private final Observable<ResponseOrError<ImmutableList<RepoModel>>> repositoriesErrorObservable;
    @NonNull
    private final PublishSubject<Object> refreshSubject = PublishSubject.create();

    @Inject
    public GithubDao(@NonNull final ApiService apiService,
                     @NonNull final Scheduler networkScheduler,
                     @NonNull final Scheduler uiScheduler) {
        this.apiService = apiService;
        this.networkScheduler = networkScheduler;
        this.uiScheduler = uiScheduler;

        repositoriesErrorObservable = apiService.getRepositories()
                .subscribeOn(networkScheduler)
                .observeOn(uiScheduler)
                .compose(ResponseOrError.<ImmutableList<RepoModel>>toResponseOrErrorObservable())
                .compose(MoreOperators.<ImmutableList<RepoModel>>repeatOnError(networkScheduler))
                .compose(MoreOperators.<ResponseOrError<ImmutableList<RepoModel>>>cacheWithTimeout(networkScheduler))
                .compose(MoreOperators.<ResponseOrError<ImmutableList<RepoModel>>>refresh(refreshSubject))
                .compose(ObservableExtensions.<ResponseOrError<ImmutableList<RepoModel>>>behaviorRefCount());

        cache = CacheBuilder.newBuilder()
                .build(new CacheLoader<BothParams<String, String>, RepoDao>() {
                    @Override
                    public RepoDao load(@Nonnull final BothParams<String, String> repoIdWithUsername) throws Exception {
                        return new RepoDao(repoIdWithUsername);
                    }
                });

        issueCache = CacheBuilder.newBuilder()
                .build(new CacheLoader<ThreeParams<String, String, String>, IsuueDao>() {
                    @Override
                    public IsuueDao load(@Nonnull final ThreeParams<String, String, String> repoIdWithUsernameAndIssueId) throws Exception {
                        return new IsuueDao(repoIdWithUsernameAndIssueId);
                    }
                });

    }

    @NonNull
    public RepoDao forRepository(@NonNull String repoId, String userId) {
        return cache.getUnchecked(new BothParams<>(userId, repoId));
    }

    @NonNull
    public IsuueDao forRepository(@NonNull String repoId, String userId, String issueId) {
        return issueCache.getUnchecked(new ThreeParams<>(userId, repoId, issueId));
    }



    @NonNull
    public Observable<ResponseOrError<ImmutableList<RepoModel>>> getRepositoriesErrorObservable() {
        return repositoriesErrorObservable;
    }

    @NonNull
    public PublishSubject<Object> getRefreshSubject() {
        return refreshSubject;
    }

    public class RepoDao {

        @NonNull
        private final Observable<ResponseOrError<ImmutableList<IssueModel>>> issuesErrorObservable;

        public RepoDao(final @NonNull BothParams<String, String> repoIdWithUsername) {
            issuesErrorObservable = apiService.getIssuesForRepo(repoIdWithUsername.param1(), repoIdWithUsername.param2())
                    .subscribeOn(networkScheduler)
                    .observeOn(uiScheduler)
                    .compose(ResponseOrError.<ImmutableList<IssueModel>>toResponseOrErrorObservable())
                    .compose(MoreOperators.<ImmutableList<IssueModel>>repeatOnError(networkScheduler))
                    .compose(MoreOperators.<ResponseOrError<ImmutableList<IssueModel>>>cacheWithTimeout(networkScheduler))
                    .compose(MoreOperators.<ResponseOrError<ImmutableList<IssueModel>>>refresh(refreshSubject))
                    .compose(ObservableExtensions.<ResponseOrError<ImmutableList<IssueModel>>>behaviorRefCount());
        }

        @NonNull
        public Observable<ResponseOrError<ImmutableList<IssueModel>>> getIssuesErrorObservable() {
            return issuesErrorObservable;
        }
    }

    public class IsuueDao {

        @NonNull
        private final Observable<ResponseOrError<ImmutableList<IssueCommentModel>>> commentsErrorObservable;

        public IsuueDao(final @NonNull ThreeParams<String, String, String> repoIdWithUsernameAndIssueId) {
            commentsErrorObservable = apiService
                    .getCommentForIssue(
                            repoIdWithUsernameAndIssueId.param1(),
                            repoIdWithUsernameAndIssueId.param2(),
                            repoIdWithUsernameAndIssueId.param3())
                    .subscribeOn(networkScheduler)
                    .observeOn(uiScheduler)
                    .compose(ResponseOrError.<ImmutableList<IssueCommentModel>>toResponseOrErrorObservable())
                    .compose(MoreOperators.<ImmutableList<IssueCommentModel>>repeatOnError(networkScheduler))
                    .compose(MoreOperators.<ResponseOrError<ImmutableList<IssueCommentModel>>>cacheWithTimeout(networkScheduler))
                    .compose(MoreOperators.<ResponseOrError<ImmutableList<IssueCommentModel>>>refresh(refreshSubject))
                    .compose(ObservableExtensions.<ResponseOrError<ImmutableList<IssueCommentModel>>>behaviorRefCount());
        }

        @NonNull
        public Observable<ResponseOrError<ImmutableList<IssueCommentModel>>> getCommentsErrorObservable() {
            return commentsErrorObservable;
        }
    }

}
