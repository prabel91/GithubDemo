package pl.prabel.githubdemo;

import android.app.Application;
import android.support.multidex.MultiDexApplication;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import java.util.List;

import dagger.ObjectGraph;
import pl.prabel.githubdemo.dagger.ApplicationModule;
import pl.prabel.githubdemo.dagger.GraphProvider;

public class MainApplication extends MultiDexApplication implements GraphProvider {

    private ObjectGraph objectGraph;

    @Override
    public void onCreate() {
        super.onCreate();
        initializeDagger();
    }

    private void initializeDagger() {
        final List<Object> modules = Lists.newArrayList();
        modules.add(new ApplicationModule(this));
        objectGraph = ObjectGraph.create(Iterables.toArray(modules, Object.class));
        objectGraph.inject(this);
    }

    public void inject(Object object) {
        objectGraph.inject(object);
    }

    public ObjectGraph getApplicationGraph() {
        return objectGraph;
    }

    public static MainApplication fromApplication(Application application) {
        return (MainApplication) application;
    }

    @Override
    public ObjectGraph getObjectGraph() {
        return objectGraph;
    }

}