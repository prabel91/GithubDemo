package pl.prabel.githubdemo.parser;

import com.google.common.collect.ImmutableMap;
import com.google.common.reflect.TypeToken;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.Map;

public class ImmutableMapDeserializer implements JsonDeserializer<ImmutableMap<?, ?>> {

    @Override
    public ImmutableMap<?, ?> deserialize(JsonElement json,
                                        Type typeOfT,
                                        JsonDeserializationContext context) throws JsonParseException {
        @SuppressWarnings("unchecked")
        final TypeToken<ImmutableMap<?, ?>> immutableListToken = (TypeToken<ImmutableMap<?, ?>>) TypeToken.of(typeOfT);
        final TypeToken<? super ImmutableMap<?, ?>> mapToken = immutableListToken.getSupertype(Map.class);
        final Map<?, ?> list = context.deserialize(json, mapToken.getType());
        return ImmutableMap.copyOf(list);
    }
}
