package pl.prabel.githubdemo.presenter.issue;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.common.collect.ImmutableList;
import com.jakewharton.rxbinding.view.RxView;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import dagger.Provides;
import pl.prabel.githubdemo.R;
import pl.prabel.githubdemo.api.model.IssueCommentModel;
import pl.prabel.githubdemo.api.model.IssueModel;
import pl.prabel.githubdemo.api.throwable.NoConnectionException;
import pl.prabel.githubdemo.dagger.ActivityModule;
import pl.prabel.githubdemo.dagger.ApplicationModule;
import pl.prabel.githubdemo.dagger.ForActivity;
import pl.prabel.githubdemo.dao.GithubDao;
import pl.prabel.githubdemo.presenter.BaseActivity;
import pl.prabel.githubdemo.rx.CustomViewAction;
import rx.functions.Func1;

public class IssueDetailsActivity extends BaseActivity {

    private static final String ISSUE_DETAILS_EXTRA = "extra_issue_model";
    private static final String REPOSITORY_ID_EXTRA = "repository_extra";
    private static final String USER_ID_EXTRA = "user_id_extra";

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.progress_view)
    View progressView;
    @Bind(R.id.container)
    View container;
    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;
    @Bind(R.id.issue_number)
    TextView issueNumber;
    @Bind(R.id.toolbar_layout)
    CollapsingToolbarLayout collapsingToolbarLayout;

    @Inject
    CommentsAdapter adapter;
    @Inject
    IssuePresenter presenter;

    public static Intent newIntent(Context context,
                                   String repositoryId,
                                   String userIdExtra,
                                   IssueModel issueModel) {
        return new Intent(context, IssueDetailsActivity.class)
                .putExtra(ISSUE_DETAILS_EXTRA, issueModel)
                .putExtra(REPOSITORY_ID_EXTRA, repositoryId)
                .putExtra(USER_ID_EXTRA, userIdExtra);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.issue_details_activity);
        ButterKnife.bind(this);

        final Intent intent = getIntent();
        final IssueModel item = intent.getParcelableExtra(ISSUE_DETAILS_EXTRA);

        setSupportActionBar(toolbar);

        final ActionBar supportActionBar = getSupportActionBar();

        assert supportActionBar != null;
        supportActionBar.setDisplayHomeAsUpEnabled(true);
        supportActionBar.setHomeButtonEnabled(true);
        collapsingToolbarLayout.setTitle(item.getTitle());

        issueNumber.setText(String.format(getString(R.string.issue_number_format), String.valueOf(item.getNumber())));

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        presenter.getProgressObservable()
                .compose(lifecycleMainObservable().<Boolean>bindLifecycle())
                .subscribe(RxView.visibility(progressView));

        presenter.getErrorObservable()
                .compose(lifecycleMainObservable().<Throwable>bindLifecycle())
                .map(new Func1<Throwable, String>() {
                    @Override
                    public String call(Throwable throwable) {
                        if (throwable != null && throwable.getCause() instanceof NoConnectionException) {
                            return getString(R.string.no_connection_error);
                        }
                        return getString(R.string.unknown_error);
                    }
                })
                .subscribe(new CustomViewAction.SnackBarErrorMessageAction(container));

        presenter.getItemsObservable()
                .compose(lifecycleMainObservable().<ImmutableList<BaseAdapterItem>>bindLifecycle())
                .map(new Func1<ImmutableList<BaseAdapterItem>, ImmutableList<BaseAdapterItem>>() {
                    @Override
                    public ImmutableList<BaseAdapterItem> call(ImmutableList<BaseAdapterItem> baseAdapterItems) {
                        return new ImmutableList.Builder<BaseAdapterItem>()
                                .add(new IssueCommentModel(item.getUser(), item.getBody(), item.getCreateAtDate()))
                                .addAll(baseAdapterItems).build();
                    }
                })
                .subscribe(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected List<Object> getModules() {
        final List<Object> modules = super.getModules();
        modules.add(new Module(this));
        return modules;
    }

    @dagger.Module(
            injects = IssueDetailsActivity.class,
            includes = ActivityModule.class,
            library = true,
            addsTo = ApplicationModule.class
    )
    public class Module {
        private IssueDetailsActivity repoActivity;

        public Module(IssueDetailsActivity repoActivity) {
            this.repoActivity = repoActivity;
        }

        @Provides
        @ForActivity
        Context provideContext() {
            return repoActivity;
        }

        @Provides
        LayoutInflater provideLayoutInflater() {
            return repoActivity.getLayoutInflater();
        }


        @Provides
        IssuePresenter provideIssuePresenter(@NonNull GithubDao githubDao) {
            final Intent intent = repoActivity.getIntent();
            final String repositoryId = intent.getStringExtra(REPOSITORY_ID_EXTRA);
            final String userIdExtra = intent.getStringExtra(USER_ID_EXTRA);
            final String issueIdExtra = String.valueOf(((IssueModel) intent.getParcelableExtra(ISSUE_DETAILS_EXTRA)).getNumber());
            return new IssuePresenter(githubDao, repositoryId, userIdExtra, issueIdExtra);
        }
    }
}
