package pl.prabel.githubdemo.presenter.issue;

import android.support.annotation.NonNull;

import com.appunite.rx.ResponseOrError;
import com.google.common.collect.ImmutableList;

import javax.inject.Inject;

import pl.prabel.githubdemo.api.model.IssueCommentModel;
import pl.prabel.githubdemo.dao.GithubDao;
import rx.Observable;
import rx.functions.Func1;

public class IssuePresenter {

    @NonNull
    private final Observable<ResponseOrError<ImmutableList<IssueCommentModel>>> issuesErrorObservable;

    @Inject
    public IssuePresenter(@NonNull final GithubDao githubDao,
                          @NonNull final String repositoryId,
                          @NonNull final String userIdExtra,
                          @NonNull final String issueId) {
        final GithubDao.IsuueDao isuueDao = githubDao.forRepository(repositoryId, userIdExtra, issueId);

        issuesErrorObservable = isuueDao.getCommentsErrorObservable();
    }

    @NonNull
    public Observable<Boolean> getProgressObservable() {
        return ResponseOrError.combineProgressObservable(
                ImmutableList.of(ResponseOrError.transform(issuesErrorObservable)));
    }

    @NonNull
    public Observable<Throwable> getErrorObservable() {
        return issuesErrorObservable
                .compose(ResponseOrError.<ImmutableList<IssueCommentModel>>onlyError());
    }

    @NonNull
    public Observable<ImmutableList<pl.prabel.githubdemo.presenter.issue.BaseAdapterItem>> getItemsObservable() {
        return issuesErrorObservable
                .compose(ResponseOrError.<ImmutableList<IssueCommentModel>>onlySuccess())
                .map(new Func1<ImmutableList<IssueCommentModel>, ImmutableList<pl.prabel.githubdemo.presenter.issue.BaseAdapterItem>>() {
                    @Override
                    public ImmutableList<pl.prabel.githubdemo.presenter.issue.BaseAdapterItem> call(ImmutableList<IssueCommentModel> issueCommentModels) {
                        return issueCommentModels.size() > 0
                                ? ImmutableList.<pl.prabel.githubdemo.presenter.issue.BaseAdapterItem>copyOf(issueCommentModels)
                                : ImmutableList.<pl.prabel.githubdemo.presenter.issue.BaseAdapterItem>of(new EmptyAdapterItem());
                    }
                });
    }

}