package pl.prabel.githubdemo.presenter.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.EditText;

import com.appunite.rx.functions.BothParams;
import com.google.common.base.Strings;
import com.jakewharton.rxbinding.view.RxView;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import dagger.Provides;
import pl.prabel.githubdemo.presenter.main.MainActivity;
import pl.prabel.githubdemo.R;
import pl.prabel.githubdemo.api.throwable.NoConnectionException;
import pl.prabel.githubdemo.content.TokenPreferences;
import pl.prabel.githubdemo.dagger.ActivityModule;
import pl.prabel.githubdemo.dagger.ApplicationModule;
import pl.prabel.githubdemo.dagger.ForActivity;
import pl.prabel.githubdemo.helper.KeyboardHelper;
import pl.prabel.githubdemo.presenter.BaseActivity;
import pl.prabel.githubdemo.rx.CustomViewAction;
import pl.prabel.githubdemo.rx.RxVisibilityUtil;
import retrofit.client.Response;
import rx.functions.Action1;
import rx.functions.Func1;

public class LoginActivity extends BaseActivity {

    @Bind(R.id.password_edit_text)
    EditText passwordEditText;
    @Bind(R.id.username_edit_text)
    EditText usernameEditText;
    @Bind(R.id.password_text_input)
    TextInputLayout passwordTextInput;
    @Bind(R.id.username_text_input)
    TextInputLayout usernameTextInput;
    @Bind(R.id.login_button)
    View loginButton;
    @Bind(R.id.progress_view)
    View progressView;
    @Bind(R.id.container)
    View container;

    @Inject
    LoginPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        RxView.clicks(loginButton)
                .compose(lifecycleMainObservable().<Void>bindLifecycle())
                .map(new MapToUsernameWithPasswordFunc())
                .filter(new ValidateUserCredentialsFunc())
                .doOnNext(RxVisibilityUtil.showView(progressView))
                .doOnNext(new KeyboardHelper.HideKeyboardAction(this))
                .subscribe(presenter.loginObserver());

        presenter.errorObservable()
                .compose(lifecycleMainObservable().<Throwable>bindLifecycle())
                .doOnNext(RxVisibilityUtil.hideView(progressView))
                .map(new Func1<Throwable, String>() {
                    @Override
                    public String call(Throwable throwable) {
                        if (throwable != null && throwable.getCause() instanceof NoConnectionException) {
                            return getString(R.string.no_connection_error);
                        }
                        return getString(R.string.invalid_login_error);
                    }
                })
                .subscribe(new CustomViewAction.SnackBarErrorMessageAction(container));

        presenter.successResponse()
                .compose(lifecycleMainObservable().<Response>bindLifecycle())
                .doOnNext(RxVisibilityUtil.hideView(progressView))
                .subscribe(new Action1<Response>() {
                    @Override
                    public void call(Response response) {
                        finish();
                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    }
                });
    }

    @Override
    protected List<Object> getModules() {
        final List<Object> modules = super.getModules();
        modules.add(new Module(this));
        return modules;
    }

    private class ValidateUserCredentialsFunc implements Func1<BothParams<String, String>, Boolean> {
        @Override
        public Boolean call(BothParams<String, String> stringStringBothParams) {
            final String username = stringStringBothParams.param1();
            final String password = stringStringBothParams.param2();
            if (Strings.isNullOrEmpty(username)) {
                usernameTextInput.setError("Username cannot be empty");
                return false;
            }
            if (Strings.isNullOrEmpty(password)) {
                passwordTextInput.setError("Password cannot be empty");
                return false;
            }

            return true;
        }
    }

    private class MapToUsernameWithPasswordFunc implements Func1<Void, BothParams<String, String>> {

        @Override
        public BothParams<String, String> call(Void aVoid) {
            return new BothParams<>(usernameEditText.getText().toString(),
                    passwordEditText.getText().toString());
        }
    }

    @dagger.Module(
            injects = LoginActivity.class,
            includes = ActivityModule.class,
            library = true,
            addsTo = ApplicationModule.class
    )
    public class Module {
        private LoginActivity loginActivity;

        public Module(LoginActivity loginActivity) {
            this.loginActivity = loginActivity;
        }

        @Provides
        @ForActivity
        Context provideContext() {
            return loginActivity;
        }

    }
}
