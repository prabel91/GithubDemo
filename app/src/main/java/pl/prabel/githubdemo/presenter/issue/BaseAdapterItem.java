package pl.prabel.githubdemo.presenter.issue;

import com.appunite.detector.SimpleDetector;

public abstract class BaseAdapterItem implements SimpleDetector.Detectable<BaseAdapterItem> {
    public static final int COMMENT_ITEM_TYPE = 0;
    public static final int EMPTY_VIEW_ITEM_TYPE = 1;

    public abstract int getItemViewType();
}
