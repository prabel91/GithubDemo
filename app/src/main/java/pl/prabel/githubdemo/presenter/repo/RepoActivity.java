package pl.prabel.githubdemo.presenter.repo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;

import com.google.common.collect.ImmutableList;
import com.jakewharton.rxbinding.view.RxView;

import java.util.List;

import javax.annotation.Nonnull;
import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import dagger.Provides;
import pl.prabel.githubdemo.R;
import pl.prabel.githubdemo.api.model.IssueModel;
import pl.prabel.githubdemo.api.throwable.NoConnectionException;
import pl.prabel.githubdemo.dagger.ActivityModule;
import pl.prabel.githubdemo.dagger.ApplicationModule;
import pl.prabel.githubdemo.dagger.ForActivity;
import pl.prabel.githubdemo.dao.GithubDao;
import pl.prabel.githubdemo.presenter.BaseActivity;
import pl.prabel.githubdemo.presenter.issue.IssueDetailsActivity;
import pl.prabel.githubdemo.rx.CustomViewAction;
import rx.Observer;
import rx.functions.Action1;
import rx.functions.Func1;

public class RepoActivity extends BaseActivity implements IssuesAdapter.Listener {

    private static final String REPO_ID_EXTRA = "repo_id_extra";
    private static final String USER_ID_EXTRA = "user_id_extra";

    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.progress_view)
    View progressView;
    @Bind(R.id.container)
    View container;

    @Inject
    IssuesAdapter adapter;
    @Inject
    SingleRepositoryPresenter presenter;

    public static Intent newIntent(@NonNull Context context,
                                   @NonNull String repoId, String login) {
        return new Intent(context, RepoActivity.class)
                .putExtra(REPO_ID_EXTRA, repoId)
                .putExtra(USER_ID_EXTRA, login);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        final String repositoryId = getIntent().getStringExtra(REPO_ID_EXTRA);
        final String userIdExtra = getIntent().getStringExtra(USER_ID_EXTRA);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(repositoryId);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        presenter.getProgressObservable()
                .compose(lifecycleMainObservable().<Boolean>bindLifecycle())
                .subscribe(RxView.visibility(progressView));

        presenter.getErrorObservable()
                .compose(lifecycleMainObservable().<Throwable>bindLifecycle())
                .map(new Func1<Throwable, String>() {
                    @Override
                    public String call(Throwable throwable) {
                        if (throwable != null && throwable.getCause() instanceof NoConnectionException) {
                            return getString(R.string.no_connection_error);
                        }
                        return getString(R.string.unknown_error);
                    }
                })
                .subscribe(new CustomViewAction.SnackBarErrorMessageAction(container));


        presenter.getIssuesObservable()
                .compose(lifecycleMainObservable().<ImmutableList<IssueModel>>bindLifecycle())
                .subscribe(adapter);

        presenter.issueClickObservable()
                .compose(lifecycleMainObservable().<IssueModel>bindLifecycle())
                .subscribe(new Action1<IssueModel>() {
                    @Override
                    public void call(IssueModel issueModel) {
                        startActivity(IssueDetailsActivity.newIntent(RepoActivity.this, repositoryId, userIdExtra, issueModel));
                    }
                });
    }

    @Override
    protected List<Object> getModules() {
        final List<Object> modules = super.getModules();
        modules.add(new Module(this));
        return modules;
    }

    @Nonnull
    @Override
    public Observer<IssueModel> clickIssueAction() {
        return presenter.issueClickObserver();
    }

    @dagger.Module(
            injects = RepoActivity.class,
            includes = ActivityModule.class,
            library = true,
            addsTo = ApplicationModule.class
    )
    public class Module {
        private RepoActivity repoActivity;

        public Module(RepoActivity repoActivity) {
            this.repoActivity = repoActivity;
        }

        @Provides
        @ForActivity
        Context provideContext() {
            return repoActivity;
        }

        @Provides
        LayoutInflater provideLayoutInflater() {
            return repoActivity.getLayoutInflater();
        }

        @Provides
        IssuesAdapter.Listener provideListener() {
            return repoActivity;
        }

        @Provides
        SingleRepositoryPresenter provideSingleRepositoryPresenter(@NonNull GithubDao githubDao) {
            final String repositoryId = repoActivity.getIntent().getStringExtra(REPO_ID_EXTRA);
            final String userIdExtra = repoActivity.getIntent().getStringExtra(USER_ID_EXTRA);
            return new SingleRepositoryPresenter(githubDao, repositoryId, userIdExtra);
        }
    }
}
