package pl.prabel.githubdemo.presenter;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.trello.rxlifecycle.ActivityEvent;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;

import dagger.ObjectGraph;
import pl.prabel.githubdemo.dagger.GraphProvider;
import pl.prabel.githubdemo.rx.LifecycleMainObservable;
import rx.subjects.BehaviorSubject;

import static com.google.common.base.Preconditions.checkNotNull;

public abstract class BaseActivity extends AppCompatActivity implements GraphProvider {

    private ObjectGraph mObjectGraph;

    @Nonnull
    private final BehaviorSubject<ActivityEvent> lifecycleSubject = BehaviorSubject.create();
    private final LifecycleMainObservable lifecycleMainObservable = new LifecycleMainObservable(
            new LifecycleMainObservable.LifecycleProviderActivity(lifecycleSubject));

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getObjectGraph().inject(this);

        lifecycleSubject.onNext(ActivityEvent.CREATE);
    }

    @Override
    protected void onStart() {
        super.onStart();

        lifecycleSubject.onNext(ActivityEvent.START);
    }

    @Override
    protected void onResume() {
        super.onResume();
        lifecycleSubject.onNext(ActivityEvent.RESUME);
    }

    @Override
    protected void onPause() {
        lifecycleSubject.onNext(ActivityEvent.PAUSE);
        super.onPause();
    }

    @Override
    protected void onStop() {
        lifecycleSubject.onNext(ActivityEvent.STOP);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        lifecycleSubject.onNext(ActivityEvent.DESTROY);
        super.onDestroy();
    }

    public LifecycleMainObservable lifecycleMainObservable() {
        return lifecycleMainObservable;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public ObjectGraph getObjectGraph() {
        if (mObjectGraph == null) {
            try {
                final GraphProvider graphProvider = checkNotNull((GraphProvider) getApplication());
                mObjectGraph = graphProvider.getObjectGraph().plus(Iterables.toArray(getModules(), Object.class));
            } catch (ClassCastException e) {
                throw new RuntimeException("Activity does not implement ActivityGraphProvider", e);
            }
        }
        return mObjectGraph;
    }

    protected List<Object> getModules() {
        final ArrayList<Object> objects = Lists.newArrayList();
        return objects;
    }
}
