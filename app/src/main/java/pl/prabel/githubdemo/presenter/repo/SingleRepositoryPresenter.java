package pl.prabel.githubdemo.presenter.repo;

import android.support.annotation.NonNull;

import com.appunite.rx.ResponseOrError;
import com.google.common.collect.ImmutableList;

import javax.inject.Inject;

import pl.prabel.githubdemo.api.model.IssueModel;
import pl.prabel.githubdemo.api.model.RepoModel;
import pl.prabel.githubdemo.dao.GithubDao;
import rx.Observable;
import rx.Observer;
import rx.subjects.PublishSubject;

public class SingleRepositoryPresenter {

    @NonNull
    private final Observable<ResponseOrError<ImmutableList<IssueModel>>> issuesErrorObservable;
    @NonNull
    private final PublishSubject<IssueModel> repoClickSubject = PublishSubject.create();

    @Inject
    public SingleRepositoryPresenter(@NonNull final GithubDao githubDao,
                                     @NonNull final String repositoryId,
                                     @NonNull final String userIdExtra) {
        final GithubDao.RepoDao repoDao = githubDao.forRepository(repositoryId, userIdExtra);

        issuesErrorObservable = repoDao.getIssuesErrorObservable();
    }

    @NonNull
    public Observable<Boolean> getProgressObservable() {
        return ResponseOrError.combineProgressObservable(
                ImmutableList.of(ResponseOrError.transform(issuesErrorObservable)));
    }

    @NonNull
    public Observable<Throwable> getErrorObservable() {
        return issuesErrorObservable
                .compose(ResponseOrError.<ImmutableList<IssueModel>>onlyError());
    }

    @NonNull
    public Observable<ImmutableList<IssueModel>> getIssuesObservable() {
        return issuesErrorObservable
                .compose(ResponseOrError.<ImmutableList<IssueModel>>onlySuccess());
    }

    @NonNull
    public Observer<IssueModel> issueClickObserver() {
        return repoClickSubject;
    }

    @NonNull
    public Observable<IssueModel> issueClickObservable() {
        return repoClickSubject;
    }

}
