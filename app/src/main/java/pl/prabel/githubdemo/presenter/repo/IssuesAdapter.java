package pl.prabel.githubdemo.presenter.repo;

import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.appunite.detector.ChangesDetector;
import com.appunite.detector.SimpleDetector;
import com.google.common.collect.ImmutableList;
import com.jakewharton.rxbinding.view.RxView;

import javax.annotation.Nonnull;
import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import pl.prabel.githubdemo.R;
import pl.prabel.githubdemo.api.model.IssueModel;
import rx.Observer;
import rx.Subscription;
import rx.functions.Action1;
import rx.functions.Func1;

public class IssuesAdapter extends RecyclerView.Adapter<IssuesAdapter.ViewHolder>
        implements Action1<ImmutableList<IssueModel>>, ChangesDetector.ChangesAdapter {

    public interface Listener {
        @Nonnull
        Observer<IssueModel> clickIssueAction();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.title)
        TextView title;
        @Bind(R.id.description)
        TextView description;

        private final View itemView;
        private Subscription subscription;

        public ViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            ButterKnife.bind(this, itemView);
        }

        public void bind(final IssueModel item) {
            title.setText(item.getTitle());
            description.setText(String.format("#%1$s opened on %2$s by %3$s", item.getNumber(),
                    String.format(item.getCreatedAt()),
                    item.getUser().getLogin()));

            subscription = RxView.clicks(itemView)
                    .map(new Func1<Void, IssueModel>() {
                        @Override
                        public IssueModel call(Void aVoid) {
                            return item;
                        }
                    })
                    .subscribe(listener.clickIssueAction());
        }

        public void recycle() {
            if (subscription != null) {
                subscription.unsubscribe();
                subscription = null;
            }
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(inflater.inflate(R.layout.issue_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onViewRecycled(ViewHolder holder) {
        holder.recycle();
        super.onViewRecycled(holder);
    }

    @Nonnull
    private ImmutableList<IssueModel> items = ImmutableList.of();
    @Nonnull
    private final ChangesDetector<IssueModel, IssueModel> changesDetector;
    @Nonnull
    private Listener listener;
    @Nonnull
    private final LayoutInflater inflater;

    @Inject
    public IssuesAdapter(@Nonnull final LayoutInflater inflater,
                         @Nonnull final Listener listener) {
        this.inflater = inflater;
        this.listener = listener;
        this.changesDetector = new ChangesDetector<>(new SimpleDetector<IssueModel>());
    }

    @Override
    public void call(ImmutableList<IssueModel> repoModelImmutableList) {
        this.items = repoModelImmutableList;
        changesDetector.newData(this, items, false);
    }
}
