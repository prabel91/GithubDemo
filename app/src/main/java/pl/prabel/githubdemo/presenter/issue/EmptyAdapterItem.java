package pl.prabel.githubdemo.presenter.issue;

import javax.annotation.Nonnull;

public class EmptyAdapterItem extends BaseAdapterItem {

    @Override
    public int getItemViewType() {
        return EMPTY_VIEW_ITEM_TYPE;
    }

    @Override
    public boolean matches(@Nonnull BaseAdapterItem item) {
        return false;
    }

    @Override
    public boolean same(@Nonnull BaseAdapterItem item) {
        return false;
    }
}
