package pl.prabel.githubdemo.presenter.issue;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.appunite.detector.ChangesDetector;
import com.appunite.detector.SimpleDetector;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.squareup.picasso.Picasso;

import javax.annotation.Nonnull;
import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import pl.prabel.githubdemo.R;
import pl.prabel.githubdemo.api.model.IssueCommentModel;
import rx.functions.Action1;

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.BaseViewHolder>
        implements Action1<ImmutableList<BaseAdapterItem>>, ChangesDetector.ChangesAdapter {

    public abstract class BaseViewHolder<T extends BaseAdapterItem> extends RecyclerView.ViewHolder {

        public BaseViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public abstract void bind(final T baseAdapterItem);
        public void recycle() {}
    }

    public class CommentViewHolder extends BaseViewHolder<IssueCommentModel> {

        @Bind(R.id.title)
        TextView title;
        @Bind(R.id.description)
        TextView description;
        @Bind(R.id.avatar)
        ImageView avatar;

        public CommentViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        public void bind(final IssueCommentModel item) {
            description.setText(Html.fromHtml(item.getBody()));
            title.setText(String.format("%1$s commented on %2$s", item.getUser().getLogin(),
                    String.format(item.getCreatedAt())));
            final String avatarUrl = item.getUser().getAvatarUrl();
            if (!Strings.isNullOrEmpty(avatarUrl)) {
                picasso.load(avatarUrl)
                        .resizeDimen(R.dimen.avatar_size, R.dimen.avatar_size)
                        .into(avatar);
            }
        }

        @Override
        public void recycle() {
            picasso.load((String)null)
                    .into(avatar);
        }
    }

    public class EmptyViewHolder extends BaseViewHolder<EmptyAdapterItem> {

        public EmptyViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        public void bind(final EmptyAdapterItem item) {
        }
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return viewType == BaseAdapterItem.COMMENT_ITEM_TYPE
                ? new CommentViewHolder(inflater.inflate(R.layout.comment_item, parent, false))
                : new EmptyViewHolder(inflater.inflate(R.layout.comments_empty_view, parent, false));
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.bind(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getItemViewType();
    }

    @Override
    public void onViewRecycled(BaseViewHolder holder) {
        holder.recycle();
        super.onViewRecycled(holder);
    }

    @Nonnull
    private ImmutableList<BaseAdapterItem> items = ImmutableList.of();
    @Nonnull
    private final ChangesDetector<BaseAdapterItem, BaseAdapterItem> changesDetector;
    @Nonnull
    private final LayoutInflater inflater;
    @NonNull
    private final Picasso picasso;

    @Inject
    public CommentsAdapter(@NonNull Picasso picasso,
                           @Nonnull final LayoutInflater inflater) {
        this.picasso = picasso;
        this.inflater = inflater;
        this.changesDetector = new ChangesDetector<>(new SimpleDetector<BaseAdapterItem>());
    }

    @Override
    public void call(ImmutableList<BaseAdapterItem> issueCommentModels) {
        this.items = issueCommentModels;
        changesDetector.newData(this, items, false);
    }
}
